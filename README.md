# REPOSITORY MATERIALE DIDATTICO UMG #


### QUI SONO CONTENUTE ESERCITAZIONI PER I CDL ###

* INGEGNERIA INFORMATICA E BIOMEDICA (BIOINGEGNERIA) A.A. 2014 - 2015


### ELENCO DETTAGLIATO ESERCITAZIONI ###

* /bioingegneria@ingegneria/aa1415/ -> esercitazione del 5/5/2015 su analisi dei segnali nel dominio del tempo e della frequenza


### ALTRI TUTORIAL ###
 * /tutorial/opencv  -> tutorial sull'acquisizione video e relativi metadati

* /tutorial/scikit-image  -> tutorial su interazione utente per creazione maschere

* /tutorial/git -> tutorial sull'aggiornamento dei repo forkati
